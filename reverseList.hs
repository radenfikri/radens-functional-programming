reverseL [] = []

reverseL xs = foldl revOp [] xs
    where revOp a b = b a

reverseList2 [] = []

reverseList2 xs = foldl (\x y -> y:x) [] xs