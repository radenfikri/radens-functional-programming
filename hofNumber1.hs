import Data.Maybe


length' a = sum (map (const 1) a)


-- map (+1) (map (+1) xs) will iterate a list and add all its element(s) by 1 and repeat it again
-- for example map (+1) (map (+1) [1,2,3,4]) will iterate the list and add each element by, thus it will be
-- [2,3,4,5] and the process is repeated so it will produces [3,4,5,6]
-- we can do map f (map g xs) as long as f and g define a function that can operated by element inside function function (such as (+1), abs, reverse etc)
-- otherwise it will produces error


applyTwice :: (a -> a) -> a -> a  
applyTwice f x = f (f x)

iter :: Integral n => n -> (a -> a) -> a -> a
iter 0 f x = x
iter n f x = iter (n-1) f (f x)

-- \n -> iter n succ is an anonymous function (function without name) that  will run the iter function
-- by taking 2 parameters, n and the number we want the succ is. for example (\n -> iter n succ) 4 5
-- will equal to iter 4 f 5 where f 5 is succ 5 (6) and will return 9

sumn :: (Eq t, Floating t, Enum t) => t -> t
sumn 0 = 1
sumn x = foldr (+) 0 (map (**2) [1..x])


mystery xs = foldr (++) [] (map sing xs)
    where
        sing x = [x]

-- this code will move a list into an empty list
-- first, it will take a list as parameter, then it will iterate each element using map and turn each element of the list become a list contain only its value
-- then it will create a list of list. lastly, it will move each element into new list and append with every element inside list of created by map function

f :: Int -> Bool
f 0 = False
f x = True

id' x = x


-- (id' . f) x will equal to id' (f x) but (id' . f 10) will produce error
-- (f . id') x will equal to f (id' x) or f x itself but (f . id' 10) will produce error
-- (id' f x) will equal to id' (f x)
-- id' will behave as its general type a -> a in (id' .f ) x and (id' f x)

composeList :: [a -> a] -> (a -> a)
composeList []     = id
composeList (f:fs) = f . composeList fs

flip' :: (a -> b -> c) -> (b -> a -> c)
flip' f = \x y -> f y x
myFlip f x y = f y x

-- flip' function flips the parameter of the functions