hof2A xs = map (+1) xs
-- equivalent with [x+1 | x <- xs]

hof2B xs ys = concat (map (\x -> map (\y -> x+y) ys) xs)
-- equivalent with [ x+y | x <- xs, y <- ys ]

hof2C xs = map (+2) (filter (>3) xs)
-- equivalent with [x+2 | x <- xs, x>3]

hof2D xys = map (\(x,_) -> x+3) xys
-- equivalent with [x+3 | (x,_) <- xys]

hof2E xys = filter (<5) (map (\(x,y) -> x+4) xys)
-- equivalent with [ x+4 | (x,y) <- xys, x+y < 5 ]

hof2F mxs = map (\(Just x) -> x+5) (filter isJust mxs)
-- equivalent with [ x+5 | Just x <- mxs ]

list2A xs = [x+3 | x <- xs]
-- equivalent with map (+3) xs

list2B xs = [x | x <- xs, x > 7]
-- equivalent with filter (>7) xs

list2C xs ys = [ x+y | x <- xs, y <- ys ]
-- equivalent with concat (map (\x -> map (\y -> x+y) ys) xs)

list2D xys = [x+y | (x,y) <- xys, (x+y) >3]
-- equivalent with filter (>3) (map (\(x,y) -> x+y) xys)