simple n a b = n * (a-b)

myFlip f x y = f y x

newSimple = myFlip simple

newSimpleB n = myFlip (simple n)

newSimpleC = (\n -> myFlip (simple n))