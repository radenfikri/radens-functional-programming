data Expr = C Float | Expr :+ Expr | Expr :- Expr | Expr :* Expr | Expr :/ Expr
    | V String | Let String Expr Expr
    deriving Show

subst :: String -> Expr -> Expr -> Expr
subst v0 e0 (V v1) = if (v0 == v1) then e0 else (V v1)
-- subst "x" (C 2) (V "x")
-- C 2
-- Contoh di atas memasukkan nilai expresi (C 2) ke String x
subst _ _ (C c) = (C c)
subst v0 e0 (e1 :+ e2) = subst v0 e0 e1 :+ subst v0 e0 e2
-- subst "x" (C 2) ((C 5) :+ (V "x"))
-- C 5.0 :+ C 2.0
-- Yang terjadi adalah:
-- subst "x" (C 2) (C 5) :+ subst "x" (C 2) (V "x")
-- maka: C 5.0 :+ C 2.0

subst v0 e0 (e1 :- e2) = subst v0 e0 e1 :- subst v0 e0 e2
subst v0 e0 (e1 :* e2) = subst v0 e0 e1 :* subst v0 e0 e2
subst v0 e0 (e1 :/ e2) = subst v0 e0 e1 :/ subst v0 e0 e2
subst v0 e0 (Let v1 e1 e2) = Let v1 e1 (subst v0 e0 e2)
-- subst "x" (C 2) (Let "y" (C 4) (V "x"))
-- Let "y" (C 4.0) (C 2.0)

evaluate :: Expr -> Float
evaluate (C x) = x
evaluate (e1 :+ e2) = evaluate e1 + evaluate e2
evaluate (e1 :- e2) = evaluate e1 - evaluate e2
evaluate (e1 :* e2) = evaluate e1 * evaluate e2
evaluate (e1 :/ e2) = evaluate e1 / evaluate e2

evaluate (Let v e0 e1) = evaluate (subst v e0 e1)
-- Konstruksi Let pada evaluate
-- 
-- evaluate (Let "x" (C 2) ((C 2) :+ (V "x")))
-- evaluate (subst "x" (C 2) ((C 2) :+ (V "x")))
-- evaluate (subst "x" (C 2) (C 2) :+ subst "x" (C 2) (V "x"))
-- evaluate (C 2.0 :+ C 2.0)
-- evaluate C 2.0 + evaluate C 2.0
-- 2.0 + 2.0
-- 4.0
--
-- evaluate (Let "x" (C 2) (((C 2) :+ (V "x")) :* (C 10)))
-- 40.0
--
-- evaluate (Let "x" (C 2) ((C 2) :+ ((V "x") :* (C 10))))
-- 22.0

evaluate (V v) = 0.0
-- Variabel apapun tanpa dideklarasikan akan bernilai 0
-- evaluate (V "x")
-- 0