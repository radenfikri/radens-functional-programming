divisor n = [x | x <- [1..n], n `mod` x == 0]

-- change the condition to x `mod` n
divisorF n = [x | x <- [1..n], x `mod` n == 0]
-- >> divisorF 12
-- >  [12]
-- This can happens because we do the `mod` operation
-- to the n, so the only number that equals to 0 when
-- operated with the mod operation is the number n itself.
-- e.g: 1 `mod` 12 = 1 and not 0
--      12 `mod` 12 = 0, so it will only return [12]