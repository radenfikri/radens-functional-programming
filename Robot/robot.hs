module Robot where

Import Array

Import List

Import Monad

Import SOEGraphics
Import Win32Misc (timeGetTime)
Import qualified GraphicsWindows as GW (getEvent)

data Direction = North 
                | East 
                | South 
                | West 
                deriving (Eq,Show,Enum)

right :: Direction -> Direction
right d = toEnum (succ (fromEnum d) `mod` 4)

data RobotState = RobotState    { position :: Position , facing :: Direction
                                , pen :: Bool , color :: Color
                                , treasure :: [Position], pocket :: Int
                                } deriving Show

type Position = (Int,Int)

updateState u = Robot (\s _ _ -> return (u s, ()))

colors :: Array Int Color
newtype Robot a = Robot (RobotState -> Grid -> Window -> IO (RobotState, a))

turnRight :: Robot ()
turnRight = updateState (\s -> s {facing = right (facing s)})

moven :: Int -> Robot ()
mapM_ :: (Foldable t, Monad m) => (a -> m b) -> t a -> m ()
--type of mapM_
moven n = mapM_ (const move) [1..n]
-- >> moven 3
-- >  mapM_ (const move) [1..3]
-- >  move >> move >> move

-- spiral movement using forloop

spiral = forloop [1..20] action
    where action = \i -> (turnRight >> moven i >> turnRight >> moven i)

-- zigzag with for loop
zigzag = forloop [1..20] action
    where action = \i -> (moven 5 >> turnRight >> move >> turnRight
    	                  >> moven 5 >> turnLeft >> move >> turnLeft)