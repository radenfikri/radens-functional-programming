-- maxTiga
maxTiga :: Integral a => a -> a -> a -> a
maxTiga a b c = max a (max b c)
-- >> maxTiga 2 3 4
-- >  max 2 (max 3 4)
-- >  max 2 4
-- >  4

-- LCM (Least Common Multiplier)
kpk :: Integral a => a -> a -> a
kpk a b = lcm a b
-- >> kpk 2 3
-- >  lcm 2 3
-- >  6

cariKPK :: Integral a => a -> a -> a
cariKPK a b = head [x | x <- [1 .. (a*b)] , x `mod` a == 0, x `mod` b == 0]

-- QuickSort
quickSort1 :: Ord a => [a] -> [a]
quickSort1 [] = []
quickSort1 (x:xs) = quickSort1 [y | y <- xs, y <= x] ++ [x] ++ quickSort1 [y | y <- xs, y > x]
--quicksort using list comprehension

quickSort2 :: Ord a => [a] -> [a]
quickSort2 [] = []
quickSort2 (x:xs) = quickSort2 (filter (<=x) xs) ++ [x] ++ quickSort2 (filter (>x) xs)
--quicksort using higher order function

-- Fungsi mergeSort:
merge :: (Ord a) => [a] -> [a] -> [a]
merge a [] = a
merge [] b = b
merge (a:as) (b:bs)
  | a < b     = a:(merge as (b:bs))
  | otherwise = b:(merge (a:as) bs)
-- fungsi di atas berfungsi untuk menggabungkan kedua belah list yang sudah dibagi menjadi
-- 2 dan kemudian digabung lagi dan dibandingkan HEAD masing-masing list baru, jika lebih
-- kecil maka akan berada di bagian depan dan diikuti oleh bagian belakang dari masing-masing sub-list

mergeSort :: (Ord a) => [a] -> [a]
mergeSort [] = []
mergeSort [a] = [a]
mergeSort a =
  merge (mergeSort firstHalf) (mergeSort lastHalf)
    where firstHalf = take ((length a) `div` 2) a
          lastHalf = drop ((length a) `div` 2) a

-- fungsi di atas berfungsi untuk membagi list menjadi 2 sublist dengan cara memotongnya di bagian
-- tengah list dan dilakukan fungsi merge kepada 2 sublist tersebut.
--
-- Contoh:
-- >> mergeSort2 [5,4,3,2]
-- >  merge (mergeSort2 [5,4]) (mergesort2 [3,2])
-- >  merge (merge [5] [4]) (merge [3] [2])
-- >  merge ([4,5]) ([2,3])
-- >  [2] : (merge [4,5] [3])
-- >  [2] : [3] : (merge [4,5] [])
-- >  [2] : [3] : [4,5]
-- >  [2,3,4,5]

-- Jumlah semua elemen dalam List dengan fold
jumlahList :: Num p => [p] -> p
jumlahList [] = 0
jumlahList xs = foldl (+) 0 xs

-- Pengurangan semua elemen dalam list dengan fold
kurangList :: Num p => [p] -> p
kurangList [] = 0
kurangList (x:xs) = foldl (-) x xs

-- Perkalian semmua elemen dalam list dengan fold
kaliList :: Num p => [p] -> p
kaliList [] = 0
kaliList xs = foldl (*) 1 xs

-- Pembagian semmua elemen dalam list dengan foldl
bagiList :: Fractional p => [p] -> p
bagiList [] = 0
bagiList (x:xs) = foldl (/) x xs

-- Nilai max pada list dengan fold
maxList :: (Ord a) => [a] -> a
maxList = foldr1 (\x acc -> if x > acc then x else acc)

-- phytagoras infinite list
phytagoras = [(x,y,z) | z <- [5 .. 100], y <- [4 .. z-1], x <- [3 .. y-1], x*x + y*y == z*z]

-- primes sieve of erasthothenes
primes = sieve [2..]
    where sieve (x:xs) = x : sieve [y | y<-xs, y `mod` x /= 0]

--fib
let fibs = 1 : 1 : [ n | x <-[2..], let n = ((fibs !! (x-1)) + (fibs !! (x-2)))]

let fibs' = 1 : 1 : zipWith (+) fibs' (tail fibs')